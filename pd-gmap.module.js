/*
* To jest jakas zmiana 2
*/

(function () {
    'use strict';

    angular.module('PDGmap', [
        'darthwade.loading'
    ]);
})();


(function () {
    'use strict';

    var PubSub = {
                    
        bind: function(ev, callback) {
            var calls = this._callbacks || (this._callbacks = {});
            (this._callbacks[ev] || (this._callbacks[ev] = [])).push(callback);
            return this;
        },
        
        trigger: function() {
            var result;
            var args = Array.prototype.slice.call(arguments, 0);
            var ev = args.shift();
            var list, calls, i, l;
            if (!(calls = this._callbacks)) {
                return this;
            }
            if (!(list = this._callbacks[ev])) {
                return this;
            }          

            for (i = 0, l = list.length; i < l; i++) {                 
                result = list[i].apply(this, args);
                if (result === false) {
                    return result;
                }
            }
            return this;
        }
    };            

    angular
        .module('PDGmap')
        .directive('mapView', MapView);

    MapView.$inject = ['$filter'];
    function MapView($filter) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            transclude: true,
            template: '<div ng-transclude></div>',
            bindToController: true,
            controller: MapViewController,
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            require: '^mapView',
            scope: {

            }
        };
        return directive;

        function link(scope, element, attrs, ctrl) {             
            var  opts = {
                center: {lat:52, lng:19},                          
                zoom: 6                
            };   

            function onPointsChange(points){
                var tmps = [];

                try {
                    tmps = angular.fromJson(attrs['points']);
                }catch(e){
                    tmps = [];
                }

                // filtrowanie w celu usunięcia z tablicy elementów które posiadają współrzędne które nie
                // są liczbami
                tmps = $filter('filter')(tmps, function(v, i){
                    if(!$.isNumeric(v.lat) || !$.isNumeric(v.lng)){
                        return false;
                    }
                    return true;
                });  
                
                angular.forEach(tmps, function(v, k){
                    v.lat = Number(v.lat);
                    v.lng = Number(v.lng);
                });
                ctrl.deleteMarkers();               
                ctrl.addMarkers(tmps);
                ctrl.createCluster();
                ctrl.fitBounds();

            }           

            ctrl.init(element[0].querySelector('#' + attrs['idMap']), opts);
            attrs.$observe('points', function(points){
                onPointsChange(points);
            });
        }
    }
    /* @ngInject */
    function MapViewController() {
        var initialized = false;
        var map = null;
        var cluster = null;
       
        var vm = this;       
        vm.markers = [];

        vm.init = function (container, opts, LatLngArray) {
            initialized = true;       
            map = new google.maps.Map(container, opts);
            vm.addMarkers(LatLngArray);   
        };

        vm.addMarker = function (LatLng) {           
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map
            });   

            if(angular.isDefined(LatLng.info)){
                var infowindow = new google.maps.InfoWindow({
                    content: LatLng.info
                });

                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }

            map.setCenter(LatLng); 
            vm.markers.push(marker); 

            return marker;
        };

        vm.addMarkers = function (LatLngArray) { 
            if(!angular.isArray(LatLngArray) || LatLngArray.length === 0){
                return false;
            }
            angular.forEach(LatLngArray, function(LatLng){
                vm.addMarker(LatLng);
            });  
        };

        vm.createCluster = function(){
            cluster = new MarkerClusterer(map, vm.markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'}
            );
        };

        vm.fitBounds = function(){
            var bounds = new google.maps.LatLngBounds();
            angular.forEach(vm.markers, function(v){
                bounds.extend(v.getPosition());
            });
            map.fitBounds(bounds);
        };

        vm.deleteMarkers = function(){
            angular.forEach(vm.markers, function(v){
                v.setMap(null);
            });
        };
        
        vm.setCenter = function(LatLng){
            map.setCenter(LatLng);
        }

        vm.setZoom = function(zoom){
            map.setZoom(zoom);
        }

        $.extend(vm, PubSub);
    }
})();


(function() {
    'use strict';

    angular
        .module('PDGmap')
        .directive('mapViewAddress', MapViewAddress);

    MapViewAddress.$inject = ['$filter', '$loading'];
    function MapViewAddress($filter, $loading) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            transclude: true,
            template: '<div ng-transclude></div>',            
            link: link,
            restrict: 'E',
            require: '^^mapView',
            scope: {
            }
        };
        return directive;
        
        function link(scope, element, attrs, ctrl) {
            var address = '';
            var coder = new google.maps.Geocoder();             
            var cityInput       = element[0].querySelector('.' + attrs['cityInput']);
            var addressInput    = element[0].querySelector('.' + attrs['addressInput']);
            var loadingKey      = attrs['dwLoadingKey'] || false;            
            var zipInput        = element[0].querySelector('.' + attrs['zipInput']);            
            var latInput        = element[0].querySelector('.' + attrs['latInput']);            
            var lngInput        = element[0].querySelector('.' + attrs['lngInput']);  



            function showLoading(){
                if(loadingKey){
                    $loading.start(loadingKey);
                }
            }

            function hideLoading(){
                if(loadingKey){
                    $loading.finish(loadingKey);
                }
            }


            var debounceFindLocation = _.debounce(findLocation, 1000);

            
            $(cityInput).on('keyup paste', debounceFindLocation);
            $(addressInput).on('keyup paste', debounceFindLocation);
            $(zipInput).on('keyup paste', debounceFindLocation);
            
            function findLocation() {

                

                var tmpAddress = [];
                var newAddress = '';
                if(cityInput){
                    tmpAddress.push(cityInput.value);
                }
                if(addressInput){
                    tmpAddress.push(addressInput.value);
                }
                if(zipInput){
                    tmpAddress.push(zipInput.value);
                }

               

                tmpAddress = $filter('filter')(tmpAddress, function(v){
                    return v.length;
                });

                newAddress = tmpAddress.join(' ');  
                if(address == newAddress){
                    return false;
                }

                showLoading();
                address = newAddress;
                if(latInput){
                    latInput.value = '';
                }
                if(lngInput){
                    lngInput.value = '';
                }
                ctrl.deleteMarkers();
                
                coder.geocode({
                    address: address    
                }, function(resp, status){
                    ctrl.deleteMarkers();
                    if(status === 'OK' && resp[0].geometry.location){                                                     
                        ctrl.addMarker(resp[0].geometry.location);                         
                        ctrl.setCenter(resp[0].geometry.location);
                        ctrl.setZoom(18);
                        latInput.value = resp[0].geometry.location.lat();
                        lngInput.value = resp[0].geometry.location.lng();

                    }else{
                        ctrl.setZoom(6);
                    }

                    hideLoading();

                });
            }   
        }
    }   
})();
